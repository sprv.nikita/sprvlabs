#-*- coding: utf-8 -*-

__author__ = "sprv"

import math

if __name__ == "__main__":
    #ввод данных
    while True:
        try:
            rad = input('Введите значение аргумента R: ')
            x,y = input('Введите кординаты точки: ').split()
            rad = float(rad)
            x = float(x)
            y = float(y)
        except ValueError:  print('проверьте ввод')
        else:
            if (rad < 0):
                print('Радиус не может быть отрицательным')
                continue
            print('введены данные: R=%f x=%f y=%f' % (rad, x, y))
            break

    if (x*x + y*y >= rad*rad and math.fabs(x)<=rad and math.fabs(y)<=rad and x<=0 and y>=0) \
        or (x*x + y*y <= rad*rad and x>=0 and y<=0):
        print('попадает в область')
    else:
        print('НЕ попадает в область')
        