#-*- coding: utf-8 -*-

__author__ = "sprv"

import math

if __name__ == "__main__":
    #ввод данных
    while True:
        try:
            x = input('Введите значение аргумента: ')
            x = float(x)
        except ValueError:  print('проверьте ввод')
        else:
            print('введены данные: %f' % (x))
            break
    
    if (x<-2):
        y = (1/3)*x + (2/3)
    elif (x>=-2 and x<2):
        y = (math.sin(x/2)/math.cos(x/2))
    elif (x>=2):
        y = x/2 - 1.5

    print("X = %.4f   Y = %.4f" % (x, y))