#-*- coding: utf-8 -*-

__author__ = "sprv"

import math

#проверка на одз
def odz(a, b):
    if (a < 1 and b > 1) or (a > 1 and b < 1): return 1
    if (a <= 0 or b <= 0 or a == 1 or b == 1): return 1
    if ((a*b)%math.pi == 0): return 1
    #все проверки пройдены
    return 0

if __name__ == "__main__":
    #ввод данных
    while True:
        try:
            a, b = input('Введите данные (a b): ').split()
            a = float(a)
            b = float(b)
        except ValueError:  print('проверьте ввод')
        else:
            print('введены данные: %f %f' % (a, b))
            if (odz(a, b)):
                print('проверка на область допустимых значений не пройдена')
            else:
                break
    
    #расчет по формулам
    z2 = math.sin(a*b + 3*math.pi/2)/math.cos(a*b + 3*math.pi/2)
    #z2 =  -1 * math.cos(a*b)/math.sin(a*b)
    tmp1 = math.pow(a, math.sqrt(math.log(b,a)))
    tmp2 = math.pow(b, math.sqrt(math.log(a,b)))
    #tmp1 - tmp2 == 0
    z1 = tmp1 - tmp2 + z2

    #вывод
    print('z1 = %.4f' % z1)
    print('z2 = %.4f' % z2)