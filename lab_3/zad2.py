#-*- coding: utf-8 -*-
#dedicated to my sunshine
__author__ = "sprv"

import math
import random

def good_shot(x, y, rad):
    if(x*x + y*y >= rad*rad and math.fabs(x)<=rad and math.fabs(y)<=rad and x<=0 and y>=0) \
        or (x*x + y*y <= rad*rad and x>=0 and y<=0):
        return True
    else:
        return False

if __name__ == "__main__":
    #ввод данных
    while True:
        try:
            print('Введите радиус мишени:')
            rad = float(input())
        except ValueError:  print('проверьте ввод')
        else:
            if (rad < 0):
                print('Радиус не может быть отрицательным')
                continue
            print('Rad = %.3f' % (rad))
            break
    
    #цикл вывода данных
    print("    X     Y     Res")
    print("----------------------")
    for n in range(10):
        x = random.uniform(-rad, rad)
        y = random.uniform(-rad, rad)
        print("%6.2f %6.2f  " %(x, y), end = ' ')
        if good_shot(x, y, rad):
            print('YES')
        else:
            print('NO')
