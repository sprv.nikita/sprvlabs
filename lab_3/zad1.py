#-*- coding: utf-8 -*-

__author__ = "sprv"

import math

def equat_y(x):
    if (x<-2):
        y = (1/3)*x + (2/3)
    elif (x>=-2 and x<2):
        y = (math.sin(x/2)/math.cos(x/2))
    elif (x>=2):
        y = x/2 - 1.5
    return y

if __name__ == "__main__":
    #ввод данных
    while True:
        try:
            print('Введите значение аргументов')
            x_beg = float(input('X_beg='))
            x_end = float(input('X_end='))
            dx = float(input('Dx='))
        except ValueError:  print('проверьте ввод')
        else:
            print('X_beg= %.3f X_end= %.3f Dx= %.3f' % (x_beg, x_end, dx))
            break
    
    #цикл вывода данных
    print('+--------+--------+')
    print('I   X    I   Y    I')
    print('+--------+--------+')
    while x_beg <= x_end:
        print('I %6.2f I %6.2f I' % (x_beg, equat_y(x_beg)))
        x_beg += dx
    print('+--------+--------+')
